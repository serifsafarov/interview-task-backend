<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     */
    public function up(): void
    {
        Schema::table('invoices', static function (Blueprint $table): void {
            $table->uuid('billed_company_id');

            $table->foreign('billed_company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     */
    public function down(): void
    {
        Schema::table('invoices', static function (Blueprint $table): void {
            $table->dropColumn('billed_company_id');

            $table->dropForeign('billed_company_id');
        });
    }
};
