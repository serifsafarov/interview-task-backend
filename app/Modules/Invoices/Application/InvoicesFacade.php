<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application;

use App\Domain\Enums\StatusEnum;
use App\Domain\Repositories\Dto\InvoiceDto;
use App\Domain\Repositories\InvoicesRepository;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Api\InvoicesFacadeInterface;
use Ramsey\Uuid\Uuid;

final readonly class InvoicesFacade implements InvoicesFacadeInterface
{
    public function __construct(
        private ApprovalFacadeInterface $approvalFacade,
        private InvoicesRepository      $repository
    )
    {
    }

    public function get(string $id): ?InvoiceDto
    {
        return $this->repository->findInvoiceById($id);
    }

    public function approve(string $id): bool
    {
        $dto = $this->repository->findInvoiceById($id);

        $res = false;

        if (!empty($dto)) {
            $res = $this->approvalFacade->approve(
                new ApprovalDto(
                    id: Uuid::fromString($id),
                    status: StatusEnum::tryFrom($dto->status),
                    entity: InvoiceDto::class
                )
            );
        }

        return $res;
    }

    public function reject(string $id): bool
    {
        $dto = $this->repository->findInvoiceById($id);

        $res = false;

        if (!empty($dto)) {
            $res = $this->approvalFacade->reject(
                new ApprovalDto(
                    id: Uuid::fromString($id),
                    status: StatusEnum::tryFrom($dto->status),
                    entity: InvoiceDto::class
                )
            );
        }

        return $res;
    }
}
