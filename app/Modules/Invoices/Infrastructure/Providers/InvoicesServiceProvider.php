<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Invoices\Api\InvoicesFacadeInterface;
use App\Modules\Invoices\Application\InvoicesFacade;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class InvoicesServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->scoped(InvoicesFacadeInterface::class, InvoicesFacade::class);
    }

    /** @return array<class-string> */
    public function provides(): array
    {
        return [
            InvoicesFacadeInterface::class,
        ];
    }
}
