<?php

namespace App\Modules\Invoices\Infrastructure\Http\Controllers;

use App\Infrastructure\Controller;
use App\Modules\Invoices\Api\InvoicesFacadeInterface;
use App\Modules\Invoices\Infrastructure\Http\Requests\InvoiceRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceController extends Controller
{
    public function __construct(
        private readonly InvoicesFacadeInterface $invoicesFacade
    )
    {
    }

    public function get(InvoiceRequest $request): JsonResource
    {
        return new JsonResource(
            $this->invoicesFacade->get($request->id)
        );
    }

    public function approve(InvoiceRequest $request): JsonResponse
    {
        return new JsonResponse(
            $this->invoicesFacade->approve($request->id)
        );
    }

    public function reject(InvoiceRequest $request): JsonResponse
    {
        return new JsonResponse(
            $this->invoicesFacade->reject($request->id)
        );
    }
}
