<?php

namespace App\Modules\Invoices\Api;

use App\Domain\Repositories\Dto\InvoiceDto;

interface InvoicesFacadeInterface
{
    public function get(string $id): ?InvoiceDto;
    public function approve(string $id): bool;
    public function reject(string $id): bool;
}
