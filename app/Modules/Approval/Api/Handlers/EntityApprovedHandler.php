<?php

namespace App\Modules\Approval\Api\Handlers;

use App\Domain\Repositories\InvoicesRepository;
use App\Modules\Approval\Api\Events\EntityApproved;

readonly class EntityApprovedHandler
{
    public function __construct(
        private InvoicesRepository $repository
    )
    {
    }

    public function handle(EntityApproved $event): void
    {
        $this->repository->markInvoiceAsApproved($event->approvalDto->id);
    }
}
