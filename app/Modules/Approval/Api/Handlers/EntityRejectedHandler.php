<?php

namespace App\Modules\Approval\Api\Handlers;

use App\Domain\Repositories\InvoicesRepository;
use App\Modules\Approval\Api\Events\EntityRejected;

readonly class EntityRejectedHandler
{
    public function __construct(
        private InvoicesRepository $repository
    )
    {
    }

    public function handle(EntityRejected $event): void
    {
        $this->repository->markInvoiceAsRejected($event->approvalDto->id);
    }
}
