<?php

namespace App\Domain\Repositories\Dto;

use Illuminate\Contracts\Support\Arrayable;

class CompanyDto implements Arrayable
{
    public function __construct(
        public string $id,
        public string $name,
        public string $street,
        public string $city,
        public string $zip,
        public string $phone,
        public string $email
    )
    {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'street' => $this->street,
            'city' => $this->city,
            'zip' => $this->zip,
            'phone' => $this->phone,
            'email' => $this->email,
        ];
    }
}
