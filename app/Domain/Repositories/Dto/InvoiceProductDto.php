<?php

namespace App\Domain\Repositories\Dto;

use Illuminate\Contracts\Support\Arrayable;

class InvoiceProductDto implements Arrayable
{
    public function __construct(
        public string $id,
        public string $name,
        public int    $price,
        public string $currency
    )
    {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'currency' => $this->currency
        ];
    }
}
