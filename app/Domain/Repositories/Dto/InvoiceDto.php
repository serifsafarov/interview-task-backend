<?php

namespace App\Domain\Repositories\Dto;

use DateTime;
use DateTimeInterface;
use Illuminate\Contracts\Support\Arrayable;

class InvoiceDto implements Arrayable
{
    /**
     * @param string $id
     * @param string $number
     * @param DateTime $date
     * @param DateTime $due_date
     * @param CompanyDto $company
     * @param CompanyDto $billed_company
     * @param array<InvoiceProductDto> $products
     * @param string $status
     */
    public function __construct(
        public string $id,
        public string $number,
        public DateTime $date,
        public DateTime $due_date,
        public CompanyDto $company,
        public CompanyDto $billed_company,
        public array $products,
        public string $status
    )
    {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'date' => $this->date->format(DateTimeInterface::ATOM),
            'due_date' => $this->due_date->format(DateTimeInterface::ATOM),
            'company' => $this->company->toArray(),
            'billed_company' => $this->billed_company->toArray(),
            'products' => array_map(function (InvoiceProductDto $product){
                return $product->toArray();
            }, $this->products),
            'status' => $this->status,
        ];
    }
}
