<?php

namespace App\Domain\Repositories\Adapters;

use App\Domain\Repositories\Dto\CompanyDto;
use App\Domain\Repositories\Dto\InvoiceDto;
use App\Domain\Repositories\Dto\InvoiceProductDto;
use App\Infrastructure\Models\Invoice;

class InvoiceModelToDtoConverter
{

    public function convert(Invoice $invoice): InvoiceDto
    {
        $products = [];
        foreach ($invoice->products as $product) {
            $products[] = new InvoiceProductDto(
                id: $product->id,
                name: $product->name,
                price: $product->price,
                currency: $product->currency
            );
        }

        return new InvoiceDto(
            id: $invoice->id,
            number: $invoice->number,
            date: $invoice->date,
            due_date: $invoice->due_date,
            company: new CompanyDto(
                id: $invoice->company->id,
                name: $invoice->company->name,
                street: $invoice->company->street,
                city: $invoice->company->city,
                zip: $invoice->company->zip,
                phone: $invoice->company->phone,
                email: $invoice->company->email
            ),
            billed_company: new CompanyDto(
                id: $invoice->billed_company->id,
                name: $invoice->billed_company->name,
                street: $invoice->billed_company->street,
                city: $invoice->billed_company->city,
                zip: $invoice->billed_company->zip,
                phone: $invoice->billed_company->phone,
                email: $invoice->billed_company->email
            ),
            products: $products,
            status: $invoice->status
        );
    }
}
