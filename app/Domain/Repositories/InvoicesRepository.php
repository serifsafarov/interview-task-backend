<?php

namespace App\Domain\Repositories;

use App\Domain\Enums\StatusEnum;
use App\Domain\Repositories\Adapters\InvoiceModelToDtoConverter;
use App\Domain\Repositories\Dto\InvoiceDto;
use App\Infrastructure\Models\Invoice;

class InvoicesRepository
{
    public function __construct(
        private InvoiceModelToDtoConverter $converter
    )
    {
    }

    public function findInvoiceById(string $id): ?InvoiceDto
    {
        /** @var ?Invoice $invoice */
        $invoice = Invoice::query()->find($id);
        return empty($invoice) ? null : $this->converter->convert($invoice);
    }

    public function markInvoiceAsApproved(string $id): bool
    {
        /** @var ?Invoice $invoice */
        $invoice = Invoice::query()->find($id);

        $res = false;
        if (!empty($invoice)) {
            $invoice->status = StatusEnum::APPROVED;
            $res = $invoice->save();
        }
        return $res;
    }

    public function markInvoiceAsRejected(string $id): bool
    {
        /** @var ?Invoice $invoice */
        $invoice = Invoice::query()->find($id);

        $res = false;
        if (!empty($invoice)) {
            $invoice->status = StatusEnum::REJECTED;
            $res = $invoice->save();
        }
        return $res;
    }
}
