<?php

namespace App\Infrastructure\Models;

use DateTime;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $name
 * @property string $street
 * @property string $city
 * @property string $zip
 * @property string $phone
 * @property string $email
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Company extends Model
{
    use HasFactory, HasUuids;
}
