<?php

namespace App\Infrastructure\Models;

use DateTime;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Collection;

/**
 * @property string $id
 * @property string $number
 * @property DateTime $date
 * @property DateTime $due_date
 * @property string $company_id
 * @property string $billed_company_id
 * @property string $status
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property Company $company
 * @property Company $billed_company
 * @property Collection $products
 */
class Invoice extends Model
{
    use HasFactory, HasUuids;

    protected $casts = [
        'date' => 'datetime',
        'due_date' => 'datetime'
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(
            Company::class,
            'company_id',
            'id'
        );
    }

    public function billed_company(): BelongsTo
    {
        return $this->belongsTo(
            Company::class,
            'billed_company_id',
            'id'
        );
    }

    public function products(): HasManyThrough
    {
        return $this->hasManyThrough(
            Product::class,
            InvoiceProductLine::class,
            'invoice_id',
            'id',
            'id',
            'product_id'
        );
    }
}
