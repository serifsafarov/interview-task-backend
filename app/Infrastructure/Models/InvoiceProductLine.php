<?php

namespace App\Infrastructure\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $invoice_id
 * @property string $product_id
 * @property integer $quantity
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class InvoiceProductLine extends Model
{
    use HasFactory;
}
