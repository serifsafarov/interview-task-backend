<?php

namespace App\Infrastructure\Models;

use DateTime;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $name
 * @property integer $price
 * @property string $currency
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Product extends Model
{
    use HasFactory, HasUuids;
}
